/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Patrick
 */
public class Hibernate {
    public SessionFactory conexao;
    
    public SessionFactory getConnection(){
        Configuration cfg = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(
        cfg.getProperties());
        conexao = cfg.buildSessionFactory(builder.build());
        return this.conexao;
    }
    
    public Session openSession(){
        return this.conexao.getCurrentSession();
        //this.conexao.openSession();
    }
    
    public void close(){
        this.conexao.close();
        this.conexao = null;
    }
}

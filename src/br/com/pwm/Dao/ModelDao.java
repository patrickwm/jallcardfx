/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Dao;


import br.com.pwm.Connection.Hibernate;
import br.com.pwm.Model.Model;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Patrick
 */
public class ModelDao {
    protected Session sessao;
    protected Hibernate hibernate;
    
    public ModelDao(){
        hibernate = new Hibernate();
        this.sessao = this.hibernate.getConnection().openSession();
    }
    
    public void add(Model a) throws Exception{
        try{
            Transaction tx = sessao.beginTransaction();
            sessao.save(a);
            tx.commit();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }finally{
            closeConnections();
        }
    }
    
    public boolean remove(Model a){
        try{
            Transaction tx = sessao.beginTransaction();
            sessao.delete(a);
            tx.commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            closeConnections();
        }
    }
    
    public boolean update(Model a){
        try{
            Transaction tx = sessao.beginTransaction();
            sessao.update(a);
            tx.commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();    
            return false;
        }finally{
            closeConnections();
        }
    }
    
    public <T extends Model> List<T> getList(Class c){
        List<T> list;
        try{
            Transaction tx = sessao.beginTransaction();
            Query consulta = sessao.createQuery("from "+c.getSimpleName());
            list = consulta.list();
            tx.commit();
            return list;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            closeConnections();
        }
    }
    
    public void closeConnections(){
        if(sessao.isOpen())
            this.sessao.close();
        if(sessao.isConnected())
            this.sessao.disconnect();
        
        this.sessao = null;
        this.hibernate.close();
        this.hibernate = null;
    }
    
}

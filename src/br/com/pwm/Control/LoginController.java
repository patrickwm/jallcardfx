/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Control;

import br.com.pwm.Model.Usuario;
import br.com.pwm.Dao.UsuarioDao;
import br.com.pwm.Util.Alerta;
import br.com.pwm.Util.OpenWindow;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

/**
 * FXML Controller class
 *
 * @author Patri
 */
public class LoginController implements Initializable,IControl {
    @FXML
    private TextField txEmail;

    @FXML
    private PasswordField txSenha;

    @FXML
    private Button btLogar;

    @FXML
    private ImageView imgLogo;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initAction();
    }    

    @Override
    public void initAction() {
        initActionKey();
        initActionMouse();
    }

    @Override
    public void initActionMouse() {
        btLogar.setOnMouseClicked((e)->{
            if(MouseButton.PRIMARY == e.getButton())
                logar();
        });
    }

    @Override
    public void initActionKey() {
        txSenha.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                logar();
        });
    }
    
    public void logar(){
        String email = this.txEmail.getText();
        String senha = this.txSenha.getText();
        alteraDisabledCampos(true);
        
        
        if(!email.isEmpty() && !senha.isEmpty()){
            UsuarioDao dao = new UsuarioDao();
            Usuario u = dao.procuraLogin(email, senha);
            if(u != null){
                OpenWindow.abreInicial(u);
                OpenWindow.fechaLogin();
            }else
                new Alerta(AlertType.ERROR, "Falha ao logar","Usuário ou senha inválidos, tente novamente!");
        } else {
            new Alerta(AlertType.ERROR, "Campos inválidos","Por favor preencha todos os campos para logar!");
        }
        alteraDisabledCampos(false);
    }
    
    private void alteraDisabledCampos(boolean value){
        new Thread(new Runnable() {
            @Override
            public void run() {
                txEmail.setDisable(value);
                txSenha.setDisable(value);
                btLogar.setDisable(value);
            }
        }).start();
    
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Control;

import br.com.pwm.Dao.UsuarioDao;
import br.com.pwm.Model.Permissao;
import br.com.pwm.Model.Usuario;
import br.com.pwm.Util.Alerta;
import br.com.pwm.Util.OpenWindow;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Patri
 */
public class CadastrarUsuarioController implements Initializable, IControl {
    @FXML
    private AnchorPane anpFundo;

    @FXML
    private TableView<Usuario> tbDados;

    @FXML
    private TableColumn<Usuario, String> clmNome;

    @FXML
    private TableColumn<Usuario, String> clmEmail;

    @FXML
    private TableColumn<Usuario, String> clmHabilitado;

    @FXML
    private TextField txNome;

    @FXML
    private TextField txEmail;

    @FXML
    private PasswordField txSenha;

    @FXML
    private CheckBox chkHabilitado;

    @FXML
    private CheckBox chkAdministrador;

    @FXML
    private CheckBox chkFinanceiro;

    @FXML
    private CheckBox chkCompras;

    @FXML
    private CheckBox chkRH;

    @FXML
    private Button btNovoUsuario;
    
    @FXML
    private Button btCadastrar;
    
    @FXML
    private Button btRemover;

    @FXML
    private Button btCancelar;

    @FXML
    private Button btVoltar;
    
    @FXML
    private Label lblID;
            
    
    private static Usuario usuarioLogado;
    private Usuario usuarioSelecionado;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initAction();
        configuraTabela();
    }    
    
    public static Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public static void setUsuarioLogado(Usuario usuarioLogado) {
        CadastrarUsuarioController.usuarioLogado = usuarioLogado;
    }

    @Override
    public void initAction() {
        initActionKey();
        initActionMouse();
        tbDados.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                usuarioSelecionado = (Usuario)newValue;
                if(usuarioSelecionado != null)
                    lblID.setText(usuarioSelecionado.getId()+"");
                btCadastrar.setText("Alterar");
                preencheDados();
            }
        });
        
    }

    @Override
    public void initActionMouse() {
        btVoltar.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                voltar();
        });
        
        btCancelar.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                limpar();
        });
        
        btNovoUsuario.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                novoUsuario();
        });
        
        btRemover.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                removeUsuario();
        });
        
        btCadastrar.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                cadastraUsuario();
        });
        
    }

    @Override
    public void initActionKey() {
        btVoltar.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                voltar();
        });
        
        btCancelar.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                limpar();
        });
        
        btNovoUsuario.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                novoUsuario();
        });
        
        btRemover.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                removeUsuario();
        });
        
        btCadastrar.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                cadastraUsuario();
        });
    }
    
    private void configuraTabela(){
        clmNome.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getNome()));
        clmEmail.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmail()));
        clmHabilitado.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getHabilitado()));
        atualizaTable();
    }
    
    private void atualizaTable(){
        tbDados.setItems(FXCollections.observableArrayList(getUsuarios()));
    }
    
    private List<Usuario> getUsuarios(){
        UsuarioDao dao = new UsuarioDao();
        return dao.getList(Usuario.class);
    }
    
    
    
    private void voltar(){
        OpenWindow.abreInicial(usuarioLogado);
        OpenWindow.fechaCadastrarUsuario();
        
    }
    
    private void limpar(){
        this.txNome.setText("");
        this.txEmail.setText("");
        this.txSenha.setText("");
        this.chkHabilitado.setSelected(false);
        this.chkAdministrador.setSelected(false);
        this.chkFinanceiro.setSelected(false);
        this.chkCompras.setSelected(false);
        this.chkRH.setSelected(false);
        this.lblID.setText("");
        this.usuarioSelecionado = null;
        this.tbDados.getSelectionModel().clearSelection();
    }
    
    private void novoUsuario(){
        limpar();
        this.btCadastrar.setText("Cadastrar");
    }
    
    private void preencheDados(){
        if(this.usuarioSelecionado != null){
            this.txNome.setText(this.usuarioSelecionado.getNome());
            this.txEmail.setText(this.usuarioSelecionado.getEmail());
            this.txSenha.setText(this.usuarioSelecionado.getSenha());
            this.chkHabilitado.setSelected(this.usuarioSelecionado.isHabilitado());
            this.chkAdministrador.setSelected(this.usuarioSelecionado.isAdministrador());
            this.chkFinanceiro.setSelected(this.usuarioSelecionado.isFinanceiro());
            this.chkCompras.setSelected(this.usuarioSelecionado.isCompras());
            this.chkRH.setSelected(this.usuarioSelecionado.isRH());
        }
    }
    
    private void removeUsuario(){
        if(usuarioSelecionado != null){
            UsuarioDao dao = new UsuarioDao();
            dao.remove(usuarioSelecionado);
            atualizaTable();
            new Alerta(AlertType.INFORMATION, "Sucesso", "Usuário removido com sucesso");
        } else
            new Alerta(AlertType.WARNING, "Atenção", "Selecione um usuário para remover");
    }
    
    private void cadastraUsuario(){
        if(isCampoValido(txNome) && isCampoValido(txSenha) && isCampoValido(txEmail)){
            int id = 0;
            if(!lblID.getText().equals(""))
                id = Integer.parseInt(lblID.getText());
            String nome = getvalorCampo(txNome);
            String email = getvalorCampo(txEmail);
            String senha = getvalorCampo(txSenha);
            boolean habilitado = getvalorCampo(chkHabilitado);
            
            boolean financeiro = getvalorCampo(chkFinanceiro);
            boolean administrador = getvalorCampo(chkAdministrador);
            boolean compras = getvalorCampo(chkCompras);
            boolean rh = getvalorCampo(chkRH);
            
            Usuario u = new Usuario(nome,email,senha,habilitado);
            if(id > 0)
                u.setId(id);
            
            List<Permissao> permissoes = new ArrayList();
            
            if(administrador)
                permissoes.add(new Permissao(1));
            if(financeiro)
                permissoes.add(new Permissao(2));
            if(compras)
                permissoes.add(new Permissao(3));
            if(rh)
                permissoes.add(new Permissao(4));
            
            u.setPermissoes(permissoes);
            UsuarioDao dao = new UsuarioDao();
            if(u.getId() <= 0){
                try {
                    dao.add(u);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                limpar();
                new Alerta(AlertType.CONFIRMATION, "Operação realizada com sucesso", "O usuário foi cadastrado com sucesso!");
            } else {
                dao.update(u);
                new Alerta(AlertType.CONFIRMATION, "Operação realizada com sucesso", "O usuário foi alterado com sucesso!");
            }
            atualizaTable();
        } else 
            new Alerta(AlertType.WARNING, "Atenção", "Por favor, preencha todos os campos");
        
    }
    
    public boolean isCampoValido(TextField tx){
        return !tx.getText().equals("");
    }
    
    public String getvalorCampo(TextField tx){
        return tx.getText();
    }
    
    public boolean getvalorCampo(CheckBox ck){
        return ck.isSelected();
    }
}

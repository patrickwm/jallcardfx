/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Control;

import br.com.pwm.Model.Usuario;
import br.com.pwm.Util.OpenWindow;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Patri
 */
public class InicialController implements Initializable, IControl {
    private static Usuario usuarioLogado;
    
    @FXML
    private Button btSair;

    @FXML
    private Button btCadastrar;
    
    @FXML
    private AnchorPane anpFundo;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initAction();
        
        if(!getUsuarioLogado().isAdministrador()){
            this.btCadastrar.setVisible(false);
            System.out.println("Fundo: "+anpFundo.getPrefWidth()+" B:"+this.btSair.getPrefWidth());
            this.btSair.setLayoutX((anpFundo.getPrefWidth()/2)-(this.btSair.getPrefWidth()/2));
        }
            
    }    

    public static Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public static void setUsuarioLogado(Usuario usuarioLogado) {
        InicialController.usuarioLogado = usuarioLogado;
    }

    @Override
    public void initAction() {
        initActionKey();
        initActionMouse();
    }

    @Override
    public void initActionMouse() {
        btSair.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                sair();
        });
        
        btCadastrar.setOnMouseClicked((e)->{
            if(e.getButton() == MouseButton.PRIMARY)
                cadastraUsuario();
        });
        
        
    }

    @Override
    public void initActionKey() {
        btSair.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                sair();
        });
        
        btCadastrar.setOnKeyPressed((e)->{
            if(e.getCode() == KeyCode.ENTER)
                cadastraUsuario();
        });
        
        
    }
    
    public void sair(){
        OpenWindow.abreLogin();
        OpenWindow.fechaInicial();
    }
    
    public void cadastraUsuario(){
        OpenWindow.abreCadastrarUsuario(usuarioLogado);
        OpenWindow.fechaInicial();
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Util;

/**
 *
 * @author Patri
 */
public enum Constantes {
    PacoteView("/br/com/pwm/View/");
    
    private String caminho;
    
    Constantes(String caminho){
        this.caminho = caminho;
    }

    public String getCaminho() {
        return caminho;
    }
    
    public String getCaminho(Class c) {
        return caminho+c.getSimpleName()+".fxml";
    }
}

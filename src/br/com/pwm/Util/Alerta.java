/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Util;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Patri
 */
public class Alerta {

    public Alerta(AlertType t, String titulo, String texto) {
        Alert a = new Alert(t);
        a.setHeaderText(titulo);
        a.setContentText(texto);
        a.show();
    }
}

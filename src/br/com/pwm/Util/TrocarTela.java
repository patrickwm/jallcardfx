/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Patri
 */
public class TrocarTela {

    public Stage abreTelaCadastro(Stage stage) {
        try {
            stage.setScene(new Scene((Parent) FXMLLoader.load(getClass().getResource("/View/Cadastro.fxml"))));
            stage.setResizable(false);
        } catch (IOException ex) {
            Logger.getLogger(TrocarTela.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stage;
    }

}

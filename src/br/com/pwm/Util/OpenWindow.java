/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Util;

import br.com.pwm.Model.Usuario;
import br.com.pwm.Principal.CadastrarUsuario;
import br.com.pwm.Principal.Inicial;
import br.com.pwm.Principal.Login;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Stage;

/**
 *
 * @author Patri
 */
public class OpenWindow {
    
    public static void abreInicial(Usuario usuario){
        try {
            new Inicial(usuario).start(new Stage());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void fechaInicial(){
        Inicial.getStage().close();
    }
    
    public static void abreLogin(){
        try {
            new Login().start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(OpenWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void fechaLogin(){
        Login.getStage().close();
    }
    
    public static void abreCadastrarUsuario(Usuario u){
        try {
            new CadastrarUsuario(u).start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(OpenWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void fechaCadastrarUsuario(){
        CadastrarUsuario.getStage().close();
    }
    
    
    
    
}

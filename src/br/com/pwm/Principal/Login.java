/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Principal;

import br.com.pwm.Util.Constantes;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Stage;
/**
 *
 * @author Patri
 */
public class Login extends Application{
    private static Stage stage;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(Constantes.PacoteView.getCaminho(Login.class)));
        Scene scene = new Scene(root);

        stage.setFullScreenExitKeyCombination(KeyCodeCombination.NO_MATCH);
        stage.setTitle("Login - JallCard");
        stage.setScene(scene);
        stage.setMaximized(false);
        
                stage.setOnCloseRequest((e)->{
            Platform.exit();
            System.exit(0);
        });
        
        stage.show();
        setStage(stage);
    }

    public static Stage getStage() {
        return Login.stage;
    }

    public static void setStage(Stage stage) {
        Login.stage = stage;
    }
}

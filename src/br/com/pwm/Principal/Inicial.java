/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pwm.Principal;

import br.com.pwm.Model.Usuario;
import br.com.pwm.Control.InicialController;
import br.com.pwm.Util.Constantes;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Stage;
/**
 *
 * @author Patri
 */
public class Inicial extends Application{
    private static Stage stage;
    
    public Inicial(Usuario usuario){
        InicialController.setUsuarioLogado(usuario);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(Constantes.PacoteView.getCaminho(Inicial.class)));
        Scene scene = new Scene(root);

        stage.setFullScreenExitKeyCombination(KeyCodeCombination.NO_MATCH);
        stage.setTitle("Inicial - JallCard");
        stage.setScene(scene);
        stage.setMaximized(false);
        
                stage.setOnCloseRequest((e)->{
            Platform.exit();
            System.exit(0);
        });
        
        stage.show();
        setStage(stage);
    }

    public static Stage getStage() {
        return Inicial.stage;
    }

    public static void setStage(Stage stage) {
        Inicial.stage = stage;
    }
}

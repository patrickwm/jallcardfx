# JallCardFX

Projeto básico de Crud com Hibernate e PostgreSQL


# Passos para funcionar o aplicativo - Windows
1. Baixar e instalar o JDK Java 8 -> https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html;
2. Baixar e instalar Netbeans 8.2 na versão Tudo -> https://netbeans.org/downloads/8.2/;
3. Baixar e instalar o PostgreSQL na versão 9.4 -> https://www.enterprisedb.com/downloads/postgres-postgresql-downloads;
    - Colocar senha "123456" sem aspas duplas
    - Porta 5432
    - Locale Default
    - Desmarcar StackBuilder e finalizar instalação
4. Baixar zip dos fontes do git e extrair eles em uma pasta de sua escolha;
4. Abrir netbeans, clicar em arquivo -> Abrir projeto e selecionar o projeto que foi extraido;
5. Abrir PGAdmin, logar com a senha que você colocou ao instalar o postgresql. Selecionar o server que ja existe e criar uma base de dados com o nome "jc" sem aspas;
7. Executar o projeto no netbeans e colocar e-mail e senha qualquer, e clicar em logar, para que as tabelas do banco sejam criadas;
8. Executar o código SQL que está no Insert.txt no postgresql, no banco criado;
9. Executar o projeto e logar com o e-mail do administrador "patrickwm@outlook.com" sem aspas e a senha "123456" sem aspas;